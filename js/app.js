var dom = document

var txt = dom.querySelector('.Form-text')
var btn = dom.querySelector('.Form-submit')
var lst = dom.querySelector('.List')

btn.addEventListener('click', handleListener)

/*
 * Declaración de handleListener
 */
function handleListener (event) {
	event.preventDefault()
	
	var txtValue = txt.value.trim()
	// "  Hola mundo        "
	// "Hola mundo"
	// "  "
	// ""

	if (!txtValue) {
		return false;
	}

	var element = dom.createElement('li')
	element.textContent = txtValue
	element.className = 'List-element'

	lst.appendChild(element)
	txt.value = ''
}